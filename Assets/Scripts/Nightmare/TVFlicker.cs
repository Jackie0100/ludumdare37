﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class TVFlicker : MonoBehaviour, IInteract
{
    public int flickerAmount = 2;
    Texture2D tex;
    float updatetimer = 0;
    [SerializeField]
    float updatetimeramount = 0.05f;
    bool isTurnedOn = true;


    public bool IsTurnedOn
    {
        get
        {
            return isTurnedOn;
        }

        set
        {
            if (!value)
            {
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.black;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                
            }
            else
            {
                this.GetComponent<Renderer>().material.color = Color.white;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
                
            }
            isTurnedOn = value;
        }
    }

    void Start()
    {
        StartCoroutine("TVFlickering");
    }
    void Update()
    {
        if (IsTurnedOn)
        {
            updatetimer += Time.deltaTime;
            if (updatetimer >= updatetimeramount)
            {
                GenerateTextures();
                updatetimer = 0;
            }
        }
    }
    void GenerateTextures()
    {
        float c = 0;
        tex = new Texture2D(160, 90);
        Color[] pix = new Color[160 * 90];
        for (int i = 0; i < 160 * 90; i++)
        {
            c = UnityEngine.Random.Range(0.0f, 1.0f);
            pix[i] = new Color(c, c, c);

        }

        tex.SetPixels(pix);
        tex.Apply();
        this.GetComponent<Renderer>().material.mainTexture = tex;
        this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", tex);
        this.GetComponent<Renderer>().material.SetFloat("_EmissionScaleUI", 1);
        DynamicGI.SetEmissive(this.GetComponent<Renderer>(), Color.white);
        DynamicGI.UpdateMaterials(this.GetComponent<Renderer>());

        c = UnityEngine.Random.Range(0.8f, 1.0f);
        this.GetComponent<Renderer>().material.color = new Color(c, c, c);
    }

    IEnumerator TVFlickering()
    {
        while(true)
        {
            //if player dies
            this.GetComponent<IInteract>().Interact();
            IsTurnedOn = !IsTurnedOn;
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.25f, flickerAmount));
        }
    }

    public void Interact()
    {
    }
    public void OnLooked()
    {
        NightmareController.instance.interactText.text = "...";
    }
}

