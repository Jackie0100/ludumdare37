﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NightmareController : MonoBehaviour {
    public static NightmareController instance;
    NightmareEffect nightmareEffect;
    public Queue<Task>[] dailyTasks;
    public ScreenFader fadecontroller;
    public Text interactText;
    public Image newsPaperImage;
    public event NightControl NightEvent;
    private float night = 1;


    public float Night
    {
        get
        {
            return night;
        }

        set
        {
            night = value;
            if (NightEvent != null)
            {
                NightEvent.Invoke(value);
            }
        }
    }

    // Use this for initialization
    void Awake () {
        
        GameObject go = GameObject.FindGameObjectWithTag("GameController");
        RenderSettings.ambientIntensity = 0;
        instance = this;

        dailyTasks = new Queue<Task>[4];

        dailyTasks[0] = new Queue<Task>();
        dailyTasks[0].Enqueue(new Task("pilljar_01", "I got the pills but it feels like ants crawling down my throat."));
        DontDestroyOnLoad(this);

        nightmareEffect = new NightmareEffect();

    }
	
	// Update is called once per frame
	void Update () {

        switch ((int)instance.Night)
        {
            case (1):
                nightmareEffect.BarricadeDoor();
                break;
            case (2):
                nightmareEffect.BarricadeDoor();
                break;
            case (3):
                nightmareEffect.RedEyes();
                break;
            case (4):
                nightmareEffect.ShadowPeople();
                break;
            case (5):
                nightmareEffect.CameraEffects();
                break;

        }

    }

    public void CheckIfTaskIsCompleted(string taskname)
    {
        if (IsTaskCompleted(taskname))
        {
            RemoveTask();
        }

    }

    public bool IsTaskCompleted(string taskname)
    {

        return GetCurrentTask != null && GetCurrentTask.objective == taskname;
    }

    public Task GetCurrentTask
    {
        get
        {
            if ((int)Night - 1 < dailyTasks[(int)Night - 1].Count)
            {
                return dailyTasks[(int)Night - 1].Peek();
            }
            return null;
        }
    }

    public Task RemoveTask()
    {
        return dailyTasks[(int)Night - 1].Dequeue();
    }

    
}
public delegate void NightControl(float night);

public interface INightEvent
{
    void UpdateNight(float day);
}
