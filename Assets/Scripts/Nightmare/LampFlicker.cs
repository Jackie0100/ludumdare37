﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class LampFlicker : MonoBehaviour, IInteract
{
    public int flickerAmount = 2;
    [SerializeField]
    Light[] lampLights;

    void Start()
    {
        StartCoroutine("LampFlickering");
    }
    void Update()
    {
        
    }

    IEnumerator LampFlickering()
    {

        while (true)
        {
            //if player dies
            this.GetComponent<IInteract>().Interact();
            yield return new WaitForSeconds(UnityEngine.Random.Range(0, flickerAmount));
        }
    }

    public void Interact()
    {
        foreach (Light l in lampLights)
        {
            l.enabled = !l.enabled;
        }
    }

    public void OnLooked()
    {
        NightmareController.instance.interactText.text = "Turn on or off the light...";
    }
}

