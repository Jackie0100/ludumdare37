﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[Serializable]
public class Task
{
    //objective is the gameobject to interact with check all_model_01 in hierachy
    public string objective { get; set; }
    public string text { get; set; }

    public Task(string objective, string text)
    {
        this.objective = objective;
        this.text = text;
    }
}
