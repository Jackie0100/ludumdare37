﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

public class InteractController : MonoBehaviour
{
	void Update ()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, 0.1f, Screen.height / 2)).origin, Camera.main.transform.TransformDirection(Vector3.forward), out hit, 2))
        {
            Debug.Log(hit.transform.name);
            Debug.DrawRay(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, 0.1f, Screen.height / 2)).origin, Camera.main.transform.TransformDirection(Vector3.forward));
            if (hit.transform.GetComponent<IInteract>() != null)
            {
                hit.transform.GetComponent<IInteract>().OnLooked();

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    foreach (IInteract i in hit.transform.GetComponents<IInteract>())
                    {
                        i.Interact();
                    }
                }
            }
            else
            {
                if (SceneManager.GetActiveScene().name == "Nightmare")
                {
                    NightmareController.instance.interactText.text = "";
                }
                else
                {
                    DayController.instance.interactText.text = "";
                }
                
            }
        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Nightmare")
            {
                NightmareController.instance.interactText.text = "";
            }
            else
            {
                DayController.instance.interactText.text = "";
            }
            
        }
    }
}
