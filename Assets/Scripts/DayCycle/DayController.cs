﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DayController : MonoBehaviour
{
    public Queue<Task>[] dailyTasks;
    public ScreenFader fadecontroller;
    public Text interactText;
    public Image newsPaperImage;
    public TodoList todolist;
    public GameObject player;
    public GameObject noose;

    public static DayController instance;

    public event DayControl DayEvent;
    public event TimeControl TimeEvent;

    private float currentTimeOfDay = 0f;
    private float day = 1;

    public float Day
    {
        get
        {
            return day;
        }

        set
        {
            day = value;
            if (DayEvent != null)
            {
                DayEvent.Invoke(value);
            }
        }
    }

    public float CurrentTimeOfDay
    {
        get
        {
            return currentTimeOfDay;
        }

        set
        {
            currentTimeOfDay = value % 24;
            if (TimeEvent != null)
            {
                TimeEvent.Invoke(value);
            }
        }
    }

    public DateTime CalculatedTime
    {
        get
        {
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Mathf.FloorToInt(currentTimeOfDay),
                Mathf.FloorToInt((currentTimeOfDay - Mathf.FloorToInt(currentTimeOfDay)) * 60), 0);
        }
    }

    private void Awake()
    {
        var obj = GameObject.FindObjectsOfType<GameObject>();

        foreach (GameObject go in obj)
        {

            if (go.GetComponent<ITimeEvent>() != null)
            {
                TimeEvent += go.GetComponent<ITimeEvent>().UpdateTime;
            }
            if (go.GetComponent<IDayEvent>() != null)
            {
                DayEvent += go.GetComponent<IDayEvent>().UpdateDay;
            }
        }

        dailyTasks = new Queue<Task>[5];

        dailyTasks[0] = new Queue<Task>();
        dailyTasks[0].Enqueue(new Task("newspaper_folded_01", "Read the newspaper."));
        dailyTasks[0].Enqueue(new Task("doormain_002", "Go take a Shower."));
        dailyTasks[0].Enqueue(new Task("sandwich_01", "Eat lunch."));
        dailyTasks[0].Enqueue(new Task("pilljar_01", "Take your medicin by the night table."));

        dailyTasks[1] = new Queue<Task>();
        dailyTasks[1].Enqueue(new Task("newspaper_folded_01", "Read the newspaper"));
        dailyTasks[1].Enqueue(new Task("doormain_002", "Go take a Shower."));
        dailyTasks[1].Enqueue(new Task("sandwich_01", "You are fat and disturbing!"));
        dailyTasks[1].Enqueue(new Task("pilljar_01", "Take your medicin by the night table."));
        

        dailyTasks[2] = new Queue<Task>();
        dailyTasks[2].Enqueue(new Task("newspaper_folded_01", "Read the newspaper"));
        dailyTasks[2].Enqueue(new Task("doormain_002", "Get lost you smelly looser!"));
        dailyTasks[2].Enqueue(new Task("sandwich_01", "So Gross... So disturbing..."));
        dailyTasks[2].Enqueue(new Task("pilljar_01", "Take your medicin by the night table."));
        dailyTasks[2].Enqueue(new Task("television_screen_01", "My show is on today!"));

        dailyTasks[3] = new Queue<Task>();
        dailyTasks[3].Enqueue(new Task("newspaper_folded_01", "Read the newspaper"));
        dailyTasks[3].Enqueue(new Task("doormain_002", "Go back to the pigs where you belong"));
        dailyTasks[3].Enqueue(new Task("sandwich_01", "Give me your lunch you fat prick!"));
        dailyTasks[3].Enqueue(new Task("pilljar_01", "I hate everything about myself..."));

        dailyTasks[4] = new Queue<Task>();
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));
        dailyTasks[4].Enqueue(new Task("", "DIE DIE DIE DIE DIE DIE DIE DIE"));



        DontDestroyOnLoad(this);

        if (instance == null)
        {
            currentTimeOfDay = 8;
            day = 1;
            instance = this;
        }
        else
        {
            currentTimeOfDay = instance.CurrentTimeOfDay;
            day = instance.Day;
            Destroy(instance.gameObject);
            instance = this;
        }

        if (DayEvent != null)
            DayEvent.Invoke(currentTimeOfDay);
        if (TimeEvent != null)
            TimeEvent.Invoke(day);
    }

    public void CheckIfTaskIsCompleted(string taskname)
    {
        if (IsTaskCompleted(taskname))
        {
            RemoveTask();
        }
        
    }

    public bool IsTaskCompleted(string taskname)
    {
        
        return GetCurrentTask != null && GetCurrentTask.objective == taskname;
    }

    //when looking at bed you make a check
    public Task GetCurrentTask
    {
        get
        {
            if (0 < dailyTasks[(int)Day - 1].Count)
            {
                return dailyTasks[(int)Day - 1].Peek();
            }
            return null;
        }
    }

    public Task RemoveTask()
    {
        todolist.StrikeOutText(dailyTasks[(int)Day - 1].Peek().text);
        return dailyTasks[(int)Day - 1].Dequeue();
    }

    public TimeofDay TimeofDay
    {
        get
        {
            if (currentTimeOfDay >= (int)TimeofDay.Night && currentTimeOfDay < (int)TimeofDay.Morning)
            {
                return TimeofDay.Night;
            }
            else if (currentTimeOfDay >= (int)TimeofDay.Morning && currentTimeOfDay < (int)TimeofDay.AfterNoon)
            {
                return TimeofDay.Morning;
            }
            else if (currentTimeOfDay >= (int)TimeofDay.AfterNoon && currentTimeOfDay < (int)TimeofDay.Evening)
            {
                return TimeofDay.AfterNoon;
            }
            else
            {
                return TimeofDay.Evening;
            }
        }
    }

    public void ChangeToNightmare()
    {

        DayController.instance.todolist.gameObject.SetActive(false);
        DayController.instance.interactText.gameObject.SetActive(false);
        SceneManager.LoadScene("Nightmare");
    }

    public void BadEndGame()
    {
        fadecontroller.FadeToBlackTransition();

        player.GetComponent<UnityStandardAssets.Characters.FirstPerson.RigidbodyFirstPersonController>().enabled = false;

        interactText.text = "Lonliness is a slow and incidious killer... Get out and explore your area for fun in your everyday life!";

        Invoke("CloseGame", 30);
    }

    void CloseGame()
    {
        Application.Quit();
    }
}

public enum TimeofDay : int
{
    Morning = 6, AfterNoon = 12, Evening = 18, Night = 0,
}


public delegate void DayControl(float day);
public delegate void TimeControl(float time);

public interface ITimeEvent
{
    void UpdateTime(float time);
}

public interface IDayEvent
{
    void UpdateDay(float day);
}