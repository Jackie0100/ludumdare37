﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightHandler : MonoBehaviour
{
    [SerializeField]
    Light pointlight;

    public void Update()
    {
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            pointlight.range = Mathf.Lerp(pointlight.range, 0f, 1 * (Time.deltaTime * 0.25f));
            pointlight.intensity = Mathf.Lerp(pointlight.intensity, 0.1f, 1 * (Time.deltaTime * 0.25f));
            this.GetComponent<Light>().intensity = Mathf.Lerp(this.GetComponent<Light>().intensity, 0.1f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.ambientIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, 0.1f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.reflectionIntensity = Mathf.Lerp(RenderSettings.reflectionIntensity, 0.1f, 1 * (Time.deltaTime * 0.25f));
        }
        if (DayController.instance.TimeofDay == TimeofDay.Morning)
        {
            RenderSettings.skybox.color = Color.black;

            pointlight.range = Mathf.Lerp(pointlight.range, 5f, 1 * (Time.deltaTime * 0.25f));
            pointlight.intensity = Mathf.Lerp(pointlight.intensity, 5f, 1 * (Time.deltaTime * 0.25f));
            this.GetComponent<Light>().intensity = Mathf.Lerp(this.GetComponent<Light>().intensity, 0.75f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.ambientIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, 0.75f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.reflectionIntensity = Mathf.Lerp(RenderSettings.reflectionIntensity, 0.6f, 1 * (Time.deltaTime * 0.25f));
        }
        if (DayController.instance.TimeofDay == TimeofDay.AfterNoon)
        {
            pointlight.range = Mathf.Lerp(pointlight.range, 10f, 1 * (Time.deltaTime * 0.25f));
            pointlight.intensity = Mathf.Lerp(pointlight.intensity, 10f, 1 * (Time.deltaTime * 0.25f));
            GetComponent<Light>().intensity = Mathf.Lerp(this.GetComponent<Light>().intensity, 2f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.ambientIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, 1.5f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.reflectionIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, 1f, 1 * (Time.deltaTime * 0.25f));
        }
        if (DayController.instance.TimeofDay == TimeofDay.Evening)
        {
            pointlight.range = Mathf.Lerp(pointlight.range, 5f, 1 * (Time.deltaTime * 0.25f));
            pointlight.intensity = Mathf.Lerp(pointlight.intensity, 5f, 1 * (Time.deltaTime * 0.25f));
            this.GetComponent<Light>().intensity = Mathf.Lerp(this.GetComponent<Light>().intensity, 0.75f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.ambientIntensity = Mathf.Lerp(RenderSettings.ambientIntensity, 0.75f, 1 * (Time.deltaTime * 0.25f));
            RenderSettings.reflectionIntensity = Mathf.Lerp(RenderSettings.reflectionIntensity, 0.6f, 1 * (Time.deltaTime * 0.25f));
        }
    }
}
