﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TodoList : MonoBehaviour, IDayEvent
{
    public void UpdateDay(float day)
    {
        this.GetComponent<Text>().text = "TODO:\n\n";
        foreach (Task t in DayController.instance.dailyTasks[(int)DayController.instance.Day - 1])
        {
            this.GetComponent<Text>().text += t.text + "\n";
        }
    }

    public void StrikeOutText(string str)
    {
        string strikedtext = "";
        foreach (char c in str)
        {
            strikedtext = strikedtext + c + '\u0336';
        }

        this.GetComponent<Text>().text = this.GetComponent<Text>().text.Replace(str, strikedtext);
    }
}
