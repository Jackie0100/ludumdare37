﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pills : MonoBehaviour, IInteract
{
    [SerializeField]
    AudioClip _clip;

    AudioSource _source;

    private void Awake()
    {
        _source = GetComponent<AudioSource>();

        _source.playOnAwake = false;
        _source.clip = _clip;
        _source.spatialBlend = 1;
    }

    public void Interact()
    {
        if (SceneManager.GetActiveScene().name == "Nightmare" && NightmareController.instance.dailyTasks[(int)NightmareController.instance.Night - 1].Count == 0)
        {
            return;
        }
        else if (SceneManager.GetActiveScene().name == "Nightmare")
        {
            _source.Play();
            StartCoroutine(NightmareController.instance.fadecontroller.FadeTransition());
            NightmareController.instance.CheckIfTaskIsCompleted(this.name);
            Invoke("DestroyTheWorld", 10);
            return;
        }
        if (SceneManager.GetActiveScene().name != "Nightmare" && DayController.instance.TimeofDay == TimeofDay.Night)
        {
            return;
        }
        if (DayController.instance.dailyTasks[(int)DayController.instance.Day - 1].Count == 0 || DayController.instance.GetCurrentTask.objective != this.name)
        {
            return;
        }

            _source.Play();
            DayController.instance.CheckIfTaskIsCompleted(this.name);
            StartCoroutine(DayController.instance.fadecontroller.FadeTransition());
            DayController.instance.CurrentTimeOfDay += 1;
    }

    public void OnLooked()
    {
        if (SceneManager.GetActiveScene().name == "Nightmare" && NightmareController.instance.dailyTasks[(int)NightmareController.instance.Night - 1].Count == 0)
        {
            NightmareController.instance.interactText.text = "Better get back to bed...";
            return;
        }
        else if (SceneManager.GetActiveScene().name == "Nightmare")
        {
            NightmareController.instance.interactText.text = "...";
            return;
        }
        if (SceneManager.GetActiveScene().name != "Nightmare" && DayController.instance.TimeofDay == TimeofDay.Night)
        {
            DayController.instance.interactText.text = "I really should get to bed, it is late...";
            return;
        }
        if (DayController.instance.dailyTasks[(int)DayController.instance.Day - 1].Count == 0 || DayController.instance.GetCurrentTask.objective != this.name)
        {
            DayController.instance.interactText.text = "I really should not take any more pills right now...";
            return;
        }

        DayController.instance.interactText.text = "The pills I should take to make me feel better";
    }

    public void DestroyTheWorld()
    {
        Destroy(GameObject.Find("All_models_01"));
    }
}
