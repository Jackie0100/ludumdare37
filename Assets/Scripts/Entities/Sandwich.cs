﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Sandwich : MonoBehaviour, IInteract
{

    [SerializeField]
    AudioClip _clip;

    AudioSource _source;

    private void Awake()
    {
        _source = GetComponent<AudioSource>();

        _source.loop = false;

        _source.playOnAwake = false;
        _source.clip = _clip;
        _source.spatialBlend = 1;
    }

    public void Interact()
    {
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            return;
        }
        //Destroy(this.gameObject);
        DayController.instance.CheckIfTaskIsCompleted(this.name);
        if (_source && _clip)
        {
            _source.Play();
        }
        //DayController.instance.CurrentTimeOfDay += 2;
    }

    public void OnLooked()
    {
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            DayController.instance.interactText.text = "I really should get to bed, it is late...";
            return;
        }
        if (DayController.instance.Day == 1)
        {
            DayController.instance.interactText.text = "A delicious sandwich I got the other day...";
        }
        else
        {
            DayController.instance.interactText.text = "Looks a little gross, but maybe it's still good...";
        }
    }
}
