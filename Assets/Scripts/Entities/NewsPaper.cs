﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class NewsPaper : MonoBehaviour, IInteract, IDayEvent
{
    [SerializeField]
    Sprite[] newspapersprites;

    [SerializeField]
    AudioClip _clip;

    AudioSource _source;

    private void Awake()
    {
        _source = GetComponent<AudioSource>();

        _source.loop = false;

        _source.playOnAwake = false;
        _source.clip = _clip;
        _source.spatialBlend = 1;
    }
    public void Interact()
    {
        if (_source && _clip)
        {
            _source.Play();
        }
        DayController.instance.newsPaperImage.sprite = newspapersprites[(int)DayController.instance.Day - 1];
        DayController.instance.newsPaperImage.gameObject.SetActive(true);
        DayController.instance.CurrentTimeOfDay += 1;
        DayController.instance.CheckIfTaskIsCompleted(this.name);
        this.gameObject.SetActive(false);

        if (DayController.instance.Day == 5)
        {
            DayController.instance.noose.SetActive(true);
        }
    }

    public void OnLooked()
    {
        DayController.instance.interactText.text = "The Daily newspaper...";
    }

    public void UpdateDay(float day)
    {
        this.gameObject.SetActive(true);
    }
}
