﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ToiletDoor : MonoBehaviour, IInteract
{
    [SerializeField]
    AudioClip _clip;

    AudioSource _source;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void Awake()
    {
        _source = GetComponent<AudioSource>();
        _source.loop = false;
        

        _source.playOnAwake = false;
        _source.clip = _clip;
        _source.spatialBlend = 1;
    }

    public void Interact()
    {
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            return;
        }
        StartCoroutine(DayController.instance.fadecontroller.FadeTransition());
        DayController.instance.CheckIfTaskIsCompleted(this.name);
        DayController.instance.CurrentTimeOfDay += 3;
        if (_source && _clip)
        {
            _source.Play();
        }
    }

    public void OnLooked()
    {
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            DayController.instance.interactText.text = "I really should get to bed, it is late...";
            return;
        }
        DayController.instance.interactText.text = "Door to my bathroom...";
    }
}
