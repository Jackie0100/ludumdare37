﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bed : MonoBehaviour, IInteract
{
    public void Interact()
    {
        if (SceneManager.GetActiveScene().name == "Nightmare")
        {
            if (NightmareController.instance.GetCurrentTask == null)
            {
                StartCoroutine(NightmareController.instance.fadecontroller.FadeTransition());
                Invoke("IncreaseNight", 3);
                NightmareController.instance.CheckIfTaskIsCompleted(this.name);
            }

        }
        else
        {
            if (DayController.instance.GetCurrentTask == null && DayController.instance.TimeofDay == TimeofDay.Night)
            {
                StartCoroutine(DayController.instance.fadecontroller.FadeTransition());
                Invoke("IncreaseDay", 3);
                DayController.instance.CheckIfTaskIsCompleted(this.name);
            }
        }


    }

    // Use this for initialization
    void Start ()
    {
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    void IncreaseNight()
    {
        NightmareController.instance.Night += 1;
    }

    void IncreaseDay()
    {
        DayController.instance.Day += 1;
        DayController.instance.ChangeToNightmare();
    }

    public void OnLooked()
    {
        if (SceneManager.GetActiveScene().name == "Nightmare")
        {
            if (NightmareController.instance.GetCurrentTask == null)
            {
                NightmareController.instance.interactText.text = "... ...";
            }
            else
            {
                NightmareController.instance.interactText.text = "I NEED MY PILLS!!!!";
            }

        }
        else
        {
            if (DayController.instance.GetCurrentTask == null && DayController.instance.TimeofDay == TimeofDay.Night)
            {
                DayController.instance.interactText.text = "This is where I sleep...";
            }
            else
            {
                DayController.instance.interactText.text = "Although I wish I could sleep I'm too busy with other stuff...";
            }
        }
        

    }
}
