﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour, IInteract
{
    [SerializeField]
    Light[] lampLights;

    public void Interact()
    {
        foreach (Light l in lampLights)
        {
            l.enabled = !l.enabled;
        }
        DayController.instance.CheckIfTaskIsCompleted(this.name);
    }

    public void OnLooked()
    {
        DayController.instance.interactText.text = "Turn on or off the light...";
    }
}
