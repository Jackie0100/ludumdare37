﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class TV : MonoBehaviour, IInteract
{
    Texture2D tex;
    float updatetimer = 0;
    [SerializeField]
    float updatetimeramount = 0.05f;
    [SerializeField]
    AudioClip _clip;

    AudioSource _source;

    bool isTurnedOn = false;

    public bool IsTurnedOn
    {
        get
        {
            return isTurnedOn;
        }

        set
        {
            if (!value)
            {
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.black;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                DayController.instance.CurrentTimeOfDay += 1;
                if (_source && _clip)
                {
                    _source.Stop();
                }

            }
            else
            {
                this.GetComponent<Renderer>().material.color = Color.white;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
                DayController.instance.CurrentTimeOfDay += 1;
                if (_source && _clip)
                {
                    _source.Play();
                }

            }
            isTurnedOn = value;
        }
    }

    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (IsTurnedOn)
        {
            updatetimer += Time.deltaTime;
            if (updatetimer >= updatetimeramount)
            {
                GenerateTextures();
                updatetimer = 0;
            }
        }
	}

    private void Awake()
    {
        _source = GetComponent<AudioSource>();

        _source.loop = true;

        _source.playOnAwake = false;
        _source.clip = _clip;
        _source.spatialBlend = 1;
    }

    void GenerateTextures()
    {
        float c = 0;
        tex = new Texture2D(160, 90);
        Color[] pix = new Color[160*90];
        for (int i = 0; i < 160 * 90; i++)
        {
                c = UnityEngine.Random.Range(0.0f, 1.0f);
                pix[i] = new Color(c, c, c);

        }

        tex.SetPixels(pix);
        tex.Apply();
        this.GetComponent<Renderer>().material.mainTexture = tex;
        this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", tex);
        this.GetComponent<Renderer>().material.SetFloat("_EmissionScaleUI", 1);
        DynamicGI.SetEmissive(this.GetComponent<Renderer>(), Color.white);
        DynamicGI.UpdateMaterials(this.GetComponent<Renderer>());

        c = UnityEngine.Random.Range(0.8f, 1.0f);
        this.GetComponent<Renderer>().material.color = new Color(c, c, c);
    }

    public void Interact()
    {
        if (DayController.instance.GetCurrentTask != null && DayController.instance.GetCurrentTask.objective != this.name && DayController.instance.dailyTasks[(int)DayController.instance.Day - 1].Count != 0)
        {
            return;
        }
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            DayController.instance.interactText.text = "I really should get to bed, it is late...";
            return;
        }
        IsTurnedOn = !IsTurnedOn;
        DayController.instance.CheckIfTaskIsCompleted(this.name);
    }

    public void OnLooked()
    {
        if (DayController.instance.dailyTasks[(int)DayController.instance.Day - 1].Count != 0)
        {
            DayController.instance.interactText.text = "I think it is better to do my daily doings first before watching tv...";
            return;
        }
        if (DayController.instance.TimeofDay == TimeofDay.Night)
        {
            DayController.instance.interactText.text = "I really should get to bed, it is late...";
            return;
        }
        DayController.instance.interactText.text = "My lovely 42 inch TV which I got in christmas present...";
    }
}
