﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class Picture : MonoBehaviour, IDayEvent
{
    void Start()
    {
        this.GetComponent<Renderer>().material.mainTexture = null;
        this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
        this.GetComponent<Renderer>().material.color = Color.white;
        this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
    }
    
    public void UpdateDay(float time)
    {
        CheckDay();
    }

    public void CheckDay()
    {
        Debug.Log("In Pictures.cs " + (int)DayController.instance.Day);
        switch ((int)DayController.instance.Day)
        {
            case (1):
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.white;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
                break;
            case (2):
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.black;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                break;
            case (3):
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.black;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                break;
            case (4):
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.black;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                break;
            case (5):
                this.GetComponent<Renderer>().material.mainTexture = null;
                this.GetComponent<Renderer>().material.SetTexture("_EmissionMap", null);
                this.GetComponent<Renderer>().material.color = Color.black;
                this.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
                break;

        }
    }
}

