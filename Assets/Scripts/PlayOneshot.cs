﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayOneshot : MonoBehaviour, IInteract {

    [SerializeField]
    AudioClip _clip;

    AudioSource _source;

    private void Awake()
    {
        _source = GetComponent<AudioSource>();

        _source.playOnAwake = false;
        _source.clip = _clip;
        _source.spatialBlend = 1;
    }

    public void Interact()
    {
        Debug.Log("Test interact");
        if (_source && _clip)
        {
            _source.Play();
        }
    }

    public void OnLooked()
    {
    }
}
