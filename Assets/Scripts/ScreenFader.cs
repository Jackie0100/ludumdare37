﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ScreenFader : MonoBehaviour
{
    [SerializeField]
    [Range(1, 100)]
    private float fadespeed;

    public IEnumerator FadeTransition()
    {
        while (GetComponent<Image>().color.a <= 1)
        {
            GetComponent<Image>().color = new Color(0, 0, 0, GetComponent<Image>().color.a + (Time.deltaTime / fadespeed));
            yield return new WaitForEndOfFrame();
        }
        yield return new WaitForSeconds(2);
        while (GetComponent<Image>().color.a >= 0)
        {
            GetComponent<Image>().color = new Color(0, 0, 0, GetComponent<Image>().color.a - (Time.deltaTime / fadespeed));
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator FadeToBlackTransition()
    {
        while (GetComponent<Image>().color.a <= 1)
        {
            GetComponent<Image>().color = new Color(0, 0, 0, GetComponent<Image>().color.a + (Time.deltaTime / fadespeed));
            yield return new WaitForEndOfFrame();
        }
    }
}
